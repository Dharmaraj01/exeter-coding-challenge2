var students = {
    student1: {
        StudentName: "Student1",
        StudentID: "S1",
        Subject1: 100,
        Subject2: 98,
        Subject3: 76,
        Subject4: 82,
        Subject5: 92
    },
    student2: {
        StudentName: "Student2",
        StudentID: "S2",
        Subject1: 92,
        Subject2: 100,
        Subject3: 98,
        Subject4: 76,
        Subject5: 82
    },
    student3: {
        StudentName: "Student3",
        StudentID: "S3",
        Subject1: 76,
        Subject2: 92,
        Subject3: 100,
        Subject4: 98,
        Subject5: 76
    },
    student4: {
        StudentName: "Student4",
        StudentID: "S4",
        Subject1: 82,
        Subject2: 76,
        Subject3: 92,
        Subject4: 100,
        Subject5: 98
    }
}

exports.create = function(req, res) {
var newStudent = req.body;
students["student" + newStudent.StudentID] = newStudent;
console.log("--->After Post, students:\n" + JSON.stringify(students, null, 7));
res.end("POST/Add: \n" + JSON.stringify(newStudent, null, 7));
};

exports.update = function(req, res) {
var StudentID = parseInt(req.params.StudentID);
var updatedStudent = req.body; 
if(students["student" + StudentID] != null){
// update data
students["student" + StudentID] = updatedStudent;
console.log("--->Update Successfully, students: \n" + JSON.stringify(students, null, 7))
// return
res.end("POST/update \n" + JSON.stringify(updatedStudent, null, 7));
}else{
res.end("Don't Exist Student:\n:" + JSON.stringify(updatedStudent, null, 7));
}
};

exports.delete = function(req, res) {
var deleteStudent = students["student" + req.params.StudentID];
delete students["student" + req.params.StudentID];
console.log("--->After deletion, student list:\n" + JSON.stringify(students, null, 7) );
res.end( "DELETE/delete: \n" + JSON.stringify(deleteStudent, null, 7));
};

exports.findAll = function(req, res) {
    console.log("--->Find All: \n" + JSON.stringify(students, null, 7));
    res.end("GET/report: \n" + JSON.stringify(students, null, 7));  
};