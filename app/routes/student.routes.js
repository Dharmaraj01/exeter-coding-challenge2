module.exports = function(app) {

    var students = require('../controllers/student.controller.js');

    app.post('/api/students', students.create);
    app.put('/api/students/:StudentID', students.update);
    app.delete('/api/students/:StudentID', students.delete);
    app.get('/api/students', students.findAll);
}